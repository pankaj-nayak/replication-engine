FROM alpine:latest as certs
RUN apk --update add ca-certificates


FROM bash:4.4
# ENV PATH=/bin
COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

ADD main /
ENTRYPOINT ["/main"]
CMD ["bash"]