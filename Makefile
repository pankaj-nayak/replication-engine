build:
	@echo "Starting to build "
	go build -o main
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
	docker build -t myapp .
push:
	echo "This is getting executed"
	docker image tag myapp:latest pankajnayak/urban-trainers:latest
	docker image push pankajnayak/urban-trainers:latest
default: build push
