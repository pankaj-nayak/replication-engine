# replication-engine
# Mulit-Cluster key-value pair replication engine using IPFS, IPFS_CLUSTER, Libp2p and Golang. 
# This network can be made to work as both COMPOSITE CLUSTERS & COLLABORATIVE CLUSTERS by changing configurations.

In this setup there are 2 IPFS-CLUSTERS with 3 nodes each with separate secret key.

Steps to run all the nodes in cluster: 
1. Clone the project. 
2. cd replication-engine
3. make build
4. docker-compose up 

Step to make RPC calls to any node in any cluster.

NOTE : 10000, 100001, 100002 belongs to CLUSTER 1 and listening on mentioned port for http request. 
similarly , 10003, 10004, 10005 belongs to CLUSTER 2. 

example commands to Add and get values : 

1. curl -X --request POST 'localhost:**10003**/add'  --data-raw '{
   "key" : "2qq37qqqq7711111",
   "Value" : "IPFS learning material"
}'

2. curl --location --request POST 'localhost:**10001**/get' \
--header 'Content-Type: application/json' \
--data-raw '{
   "key" : "2qq377711111"
}'

