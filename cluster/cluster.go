package cluster

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"

	mrand "math/rand"

	"github.com/ipfs/go-cid"
	"github.com/ipfs/ipfs-cluster/api"
	"github.com/ipfs/ipfs-cluster/api/rest/client"
	"github.com/libp2p/go-libp2p"
	crypto "github.com/libp2p/go-libp2p-core/crypto"
	"github.com/multiformats/go-multiaddr"
)

const mapID = "uniquenameformap"

// IpfsCluster ... Clint for IPFS-CLUSTER
type IpfsCluster struct {
	cluster client.Client
}

var instance *IpfsCluster
var once sync.Once

// InitCluster ... Initialize cluster client with port
func InitCluster(sourcePort, identifier int) {
	getInstanceWithSourcePort(sourcePort, identifier)
}

// GetInstance ...
func GetInstance() *IpfsCluster {
	sourcePort := 9999
	return getInstanceWithSourcePort(sourcePort, 0)
}

//GetInstanceWithSourcePort ... return ipfs-cluter client instance
func getInstanceWithSourcePort(sourcePort int, identifier int) *IpfsCluster {
	once.Do(func() {
		var r io.Reader
		r = mrand.New(mrand.NewSource(int64(sourcePort)))
		prvKey, _, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, r)

		if err != nil {
			panic(err)
		}

		if _, err := os.Stat("/data/p2pnode/.key"); os.IsNotExist(err) {
			// path to key file does not exist
			fmt.Println("Path to .key does not exist")

			x, err := crypto.MarshalPrivateKey(prvKey)
			fmt.Printf("pivate key :  %x\n ", x)

			err = ioutil.WriteFile("/data/p2pnode/.key", x, 0644)
			if err != nil {
				panic(err)
			}
		}

		y, err := ioutil.ReadFile("/data/p2pnode/.key")

		savedPrvKey, err := crypto.UnmarshalPrivateKey((y))
		if err != nil {
			panic(err)
		}

		sourceMultiAddr, _ := multiaddr.NewMultiaddr(fmt.Sprintf("/ip4/127.0.0.1/tcp/%d", 0))

		fmt.Println("sourceMultiaddress:", sourceMultiAddr)
		// libp2p.New constructs a new libp2p Host.
		// Other options can be added here.
		host, err := libp2p.New(
			context.Background(),
			libp2p.ListenAddrs(sourceMultiAddr),
			libp2p.Identity(savedPrvKey),
		)
		if err != nil {
			panic(err)
		}

		// pay attention to identifier and source port
		s := fmt.Sprintf("/dns4/cluster%d/tcp/%v/ipfs/%s", identifier, sourcePort, host.ID().Pretty())
		peerAddr, err := multiaddr.NewMultiaddr(s)
		if err != nil {
			log.Println(err)
			panic(err)
		}
		fmt.Println("bhai kuch to ho", s, peerAddr)
		clusterClient, err := client.NewDefaultClient(&client.Config{
			APIAddr:  peerAddr,
			LogLevel: "info",
		})

		instance = &IpfsCluster{cluster: clusterClient}
	})
	return instance
}

// GetMapCidByPinName ... returns CID of key/Value pair map by looking at pinset by name
func (c IpfsCluster) GetMapCidByPinName() (mapCid cid.Cid, found bool, retErr error) {

	pinset, err := c.cluster.Allocations(context.Background(), 1)
	if err != nil {
		log.Println(err)
		retErr = err
		return
	}

	found = false
	for value := range pinset {
		pin := pinset[value]

		// checking pin by tagged name
		if strings.Contains(strings.ToLower(pin.Name), strings.ToLower(mapID)) {
			// fmt.Println("value: ", pin.Cid)
			mapCid = pin.Cid
			found = true
			return
		}
	}
	return
}

//PinCid ... is used for pinning a CID using ipfs-cluster
func (c IpfsCluster) PinCid(cid cid.Cid) (res *api.Pin, err error) {

	pinOpts := api.PinOptions{}
	pinOpts.Name = mapID
	res, err = c.cluster.Pin(context.Background(), cid, pinOpts)
	if err != nil {
		log.Println(err)
	}
	// fmt.Println("result : ", res)
	return

}

//UpdatePin ... is used for updating a pin in ipfs-cluster
func (c IpfsCluster) UpdatePin(currentCid, newCid cid.Cid) (retValue *api.Pin, err error) {

	pinOpts := api.PinOptions{}
	pinOpts.Name = "uniquenameformap"
	pinOpts.PinUpdate = newCid

	retValue, err = c.cluster.Pin(context.Background(), currentCid, pinOpts)
	if err != nil {
		log.Println(err)
	}
	// fmt.Println("result : ", retValue)
	return

}

//RemovePin ... is used for unpinning a CLID from ipfs-cluster
func (c IpfsCluster) RemovePin(cid cid.Cid) (res *api.Pin, err error) {

	res, err = c.cluster.Unpin(context.Background(), cid)
	if err != nil {
		log.Println(err)
		panic(err)
	}
	// fmt.Println("result : ", res)
	return
}
