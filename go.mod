module replication-engine

go 1.14

require (
	github.com/crossdock/crossdock-go v0.0.0-20160816171116-049aabb0122b // indirect
	github.com/ipfs/go-cid v0.0.7
	github.com/ipfs/go-ipfs-api v0.0.3
	github.com/ipfs/ipfs-cluster v0.13.0
	github.com/joho/godotenv v1.3.0
	github.com/libp2p/go-libp2p v0.11.0
	github.com/libp2p/go-libp2p-core v0.6.1
	github.com/libp2p/go-libp2p-crypto v0.1.0
	github.com/multiformats/go-multiaddr v0.3.1
	github.com/uber/jaeger-client-go v2.15.0+incompatible
)
