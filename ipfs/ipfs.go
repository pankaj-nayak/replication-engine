package ipfs

import (
	"fmt"
	"os"
	"sync"

	shell "github.com/ipfs/go-ipfs-api"
)

//Ipfs ... ipfs client
type Ipfs struct {
	sh *shell.Shell
}

var instance *Ipfs
var once sync.Once

// InitShell ... used to initialize ipfs client with specified port
func InitShell(idenifier int) {
	getShellInstanceWithPort(idenifier)
}

// GetShellInstance ...
func GetShellInstance() *Ipfs {
	url := 0
	return getShellInstanceWithPort(url)
}

func getShellInstanceWithPort(identifier int) *Ipfs {
	once.Do(func() {
		s := fmt.Sprintf("ipfs%d:5001", identifier)
		sh := shell.NewShell(s)
		instance = &Ipfs{sh: sh}
	})
	return instance
}

// Set writes key-value data
func (s *Ipfs) Set(data []byte) string {
	// Dag PUT operation which will return the CID for futher access or pinning etc.
	cid, err := s.sh.DagPut(data, "json", "cbor")
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %s", err)
		os.Exit(1)
	}
	return cid
}

// GetWithKey handles READ operations of a DAG entry by CID, returning the corresponding value
func (s *Ipfs) GetWithKey(ref, key string) (out interface{}, err error) {
	err = s.sh.DagGet(ref+"/"+key, &out)
	return
}

// Get ...
func (s *Ipfs) Get(ref string) (out interface{}, err error) {
	err = s.sh.DagGet(ref, &out)
	return
}
