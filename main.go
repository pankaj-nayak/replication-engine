package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	http "net/http"
	"os"
	"replication-engine/cluster"
	ipfs "replication-engine/ipfs"

	"github.com/ipfs/go-cid"
)

//welcome home page
func homePageHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method) //get	request	method
	if r.Method == "GET" {
		io.WriteString(w, "Welcome to multi cluster replication engine")
		// w.Write("Welcome to multi-cluster replication engine")
	} else {
		r.ParseForm()
	}
}

// KeyValue ...input key value
type KeyValue struct {
	Key   string
	Value string
}

// Key ... input get key
type Key struct {
	Key string
}

func addHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method) //get	request	method
	if r.Method == "GET" {
		for k, v := range r.URL.Query() {
			fmt.Printf("%s: %s\n", k, v)
		}
	} else {
		decoder := json.NewDecoder(r.Body)
		var t KeyValue
		err := decoder.Decode(&t)

		if err != nil {
			panic(err)
		}
		fmt.Println(t.Key, t.Value)

		cluster := cluster.GetInstance()

		// Get Map
		currentCid, found, err := cluster.GetMapCidByPinName()
		if err != nil {
			panic(err)
		}
		if found == false {
			keyValueMap := make(map[string]interface{})
			keyValueMap[t.Key] = t.Value

			// Converting into JSON object
			inputJSON, err := json.Marshal(keyValueMap)
			if err != nil {
				fmt.Println(err)
			}

			// marshaled JSON object before sending it to IPFS
			jsonObj := string(inputJSON)
			fmt.Println("JSON object before sending to IPFS:")
			fmt.Println(jsonObj)

			// adding map to IPFS
			c := ipfs.GetShellInstance().Set(inputJSON)

			retValue, err := cid.Parse(c)
			if err != nil {
				fmt.Println(err)
				panic(err)
			}

			// Pinning the map cid using ipfs-cluster
			_, err = cluster.PinCid(retValue)
			if err != nil {
				fmt.Println(err)
				panic(err)
			}
			// updating current map CID
			currentCid = retValue
			fmt.Println("new map CID: ", currentCid.String())
			io.WriteString(w, currentCid.String())

		} else {
			fmt.Println("existing map CID: ", currentCid.String())
			// retreiving map using CID
			res, err := ipfs.GetShellInstance().Get(currentCid.String())
			if err != nil {
				fmt.Println(err)
				panic(err)
			}

			mymap, ok := res.(map[string]interface{})
			if ok {
				fmt.Println("we are doing ok", mymap)
			} else {
				panic("invalid map")
			}

			// updating new key value pair
			mymap[t.Key] = t.Value
			jsonObj, err := json.Marshal(mymap)

			newCid := ipfs.GetShellInstance().Set(jsonObj)
			fmt.Println(" new CID : ", newCid)

			// converting string to cid.Cid
			c, err := cid.Parse(newCid)
			res, err = cluster.RemovePin(currentCid)
			if err != nil {
				fmt.Fprintf(os.Stderr, "error: %s", err)
				os.Exit(1)
			}
			res, err = cluster.PinCid(c)
			if err != nil {
				fmt.Println(err)
				panic(err)
			}
			io.WriteString(w, newCid)
		}
	}
}

func getHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method) //get	request	method
	if r.Method == "GET" {
		fmt.Println("Inside Get method")
	} else {
		decoder := json.NewDecoder(r.Body)
		var t Key
		err := decoder.Decode(&t)

		if err != nil {
			panic(err)
		}
		fmt.Println(t.Key)

		cluster := cluster.GetInstance()

		// Get Map
		currentCid, found, err := cluster.GetMapCidByPinName()
		if err != nil {
			panic(err)
		}
		if found == false {
			responseToUser := "Map don't exist, createone first by using add method"
			fmt.Println(responseToUser)
			io.WriteString(w, responseToUser)
			return
		}
		fmt.Println("existing map CID: ", currentCid.String())
		// retreiving map using CID
		res, err := ipfs.GetShellInstance().Get(currentCid.String())
		if err != nil {
			fmt.Println(err)
			panic(err)
		}

		mymap, ok := res.(map[string]interface{})
		if ok {
			fmt.Println("we are doing ok", mymap)
		} else {
			panic("invalid map")
		}

		value, ok := mymap[t.Key]
		if ok {
			x, err := json.Marshal(value)
			if err != nil {
				panic(err)
			}
			io.WriteString(w, string(x))
		} else {
			io.WriteString(w, "Value with given key don't exist")
		}
	}
}

func main() {
	identifier := flag.Int("id", 0, "identifier for ipfs and ipfs cluster")
	sourcePort := flag.Int("sp", 0, "Source port number")

	flag.Parse()

	ipfs.InitShell(*identifier)
	cluster.InitCluster(*sourcePort, *identifier)

	//binding handler functions with end point
	http.HandleFunc("/", homePageHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/get", getHandler)

	fmt.Printf("Listening on ######### %d ############\n", 10000)
	http.ListenAndServe(":10000", nil)
	select {}
}
